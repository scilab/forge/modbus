/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2011 - DIGITEO - Allan CORNET
 * Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <string.h>
#include <errno.h>

#include "modbus-tcp.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "sci_malloc.h"
#include "localization.h"
/* ========================================================================== */
int sci_modbus_readBits(char *fname, void * pvApiCtx)
{
    SciErr sciErr;
    int errsv = 0;
    modbus_t *pstrMod = NULL;
    int *piAddr1 = NULL;
    int *piAddr2 = NULL;
    int *piAddr3 = NULL;

    double dblNbBits = 0;
    int iNbBits = 0;
    double dblAddr = 0;
    int iAddr = 0;

    double *pdblOut = NULL;
    uint8_t *pcRead = NULL;

    CheckInputArgument(pvApiCtx, 3, 3);
    CheckOutputArgument(pvApiCtx, 1, 2);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isPointerType(pvApiCtx, piAddr1))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A pointer expected.\n"), fname, 1);
        return 1;
    }

    sciErr = getPointer(pvApiCtx, piAddr1, (void**)&pstrMod);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (pstrMod == NULL)
    {
        Scierror(999, _("%s: Invalid pointer #%d.\n"), fname, 1);
        return 1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr2) || !isScalar(pvApiCtx, piAddr2))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 2);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr2, &dblAddr))
    {
        return 1;
    }

    iAddr = (int)dblAddr;

    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddr3);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr3) || !isScalar(pvApiCtx, piAddr3))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A integer expected.\n"), fname, 3);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr3, &dblNbBits))
    {
        return 1;
    }

    iNbBits = (int)dblNbBits;

    pcRead = (uint8_t *)MALLOC(sizeof(uint8_t) * iNbBits);
    if (pcRead)
    {
        int i = 0;
        int rc = 0;
        memset(pcRead, 0x00, iNbBits * sizeof(uint8_t));
        errno = 0;
        rc = modbus_read_bits(pstrMod, iAddr, iNbBits, pcRead);
        errsv = errno;
        errno = 0;
        if (rc == 1 && nbOutputArgument(pvApiCtx) != 2)
        {
            FREE(pcRead);
            Scierror(999, "%s: %s\n", fname, modbus_strerror(errsv));
            return 1;
        }
        else if(rc == -1)
        {
            if (createEmptyMatrix(pvApiCtx, nbInputArgument(pvApiCtx) + 1))
            {
                FREE(pcRead);
                Scierror(999, _("%s: Memory error.\n"), fname);
                return 1;
            }

            if (createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, errsv))
            {
                FREE(pcRead);
                Scierror(999, _("%s: Memory error.\n"), fname);
                return 1;
            }

            AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
            AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;
            ReturnArguments(pvApiCtx);
            return 0;
        }

        //alloc output variable
        sciErr = allocMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, iNbBits, 1, &pdblOut);
        if (sciErr.iErr)
        {
            FREE(pcRead);
            printError(&sciErr, 0);
            return 1;
        }

        //fill output variable
        for(i = 0 ; i < iNbBits ; i++)
        {
            pdblOut[i] = pcRead[i];
        }

        FREE(pcRead);

        AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
        if(nbOutputArgument(pvApiCtx) == 2)
        {
            if (createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, errsv))
            {
                Scierror(999, _("%s: Memory error.\n"), fname);
                return 1;
            }
            AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;
        }

       ReturnArguments(pvApiCtx);
    }
    else
    {
        Scierror(999, _("%s: Memory allocation error.\n"), fname);
        return 1;
    }

    return 0;
}
