/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2011 - DIGITEO - Allan CORNET
 * Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "modbus.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "sci_malloc.h"
#include "localization.h"
/* ========================================================================== */
int sci_modbus_close(char *fname, void * pvApiCtx)
{
    SciErr sciErr;
    modbus_t *pstrMod = NULL;
    int *piAddr1 = NULL;

    CheckInputArgument(pvApiCtx, 1, 1);
    CheckOutputArgument(pvApiCtx, 1, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isPointerType(pvApiCtx, piAddr1))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A pointer expected.\n"), fname, 1);
        return 1;
    }

    sciErr = getPointer(pvApiCtx, piAddr1, (void**)&pstrMod);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (pstrMod)
    {
        modbus_close(pstrMod);
        ReturnArguments(pvApiCtx);
    }
    else
    {
        Scierror(999, _("%s: Invalid pointer #%d.\n"), fname, 1);
        return 1;
    }

    return 0;
}
/* ========================================================================== */
