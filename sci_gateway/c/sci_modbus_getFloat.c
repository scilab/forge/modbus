/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2011 - DIGITEO - Allan CORNET
 * Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "modbus.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "sci_malloc.h"
#include "localization.h"

int sci_modbus_getFloat(char *fname, void * pvApiCtx)
{
    SciErr sciErr;

    int iBigEndian = 1;
    double *pdblVal = NULL;
    int *piAddr1 = NULL;
    int iRows1 = 0;
    int iCols1 = 0;
    int iSize = 0;

    int* piAddr2 = NULL;

    double* pdblRetVal = NULL;
    int i = 0;
    int index = 0;

    CheckInputArgument(pvApiCtx, 1, 2);
    CheckOutputArgument(pvApiCtx, 1, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr1))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A matrix expected.\n"), fname, 1);
        return 1;
    }

    sciErr = getMatrixOfDouble(pvApiCtx, piAddr1, &iRows1, &iCols1, &pdblVal);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    iSize = iRows1 * iCols1;
    if(iSize % 2)
    {//check even
        Scierror(999, _("%s: Wrong size for input argument #%d: even size expected.\n"), fname, 1);
        return 1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isBooleanType(pvApiCtx, piAddr2))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A boolean expected.\n"), fname, 2);
        return 1;
    }

    if(getScalarBoolean(pvApiCtx, piAddr2, &iBigEndian))
    {
        Scierror(999, _("%s: Wrong size for input argument #%d: A boolean expected.\n"), fname, 2);
        return 1;
    }

    sciErr = allocMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, (int)(iSize / 2), 1, &pdblRetVal);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if(iBigEndian)
    {
        for(i = 0 ; i < iSize ; i = i + 2)
        {
            uint16_t psVal[2] = {pdblVal[i+1], pdblVal[i]};
            pdblRetVal[index++] = (double)modbus_get_float(psVal);
        }
    }
    else
    {
        for(i = 0 ; i < iSize ; i = i + 2)
        {
            uint16_t psVal[2] = {pdblVal[i], pdblVal[i+1]};
            pdblRetVal[index++] = (double)modbus_get_float(psVal);
        }
    }
    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
    ReturnArguments(pvApiCtx);
    return 0;
}
