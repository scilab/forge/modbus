// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function varargout = modbus_writeBit(modbusContext, address, value)
    [lhs, rhs] = argn(0);
    if lhs <> [1 2] then
        error(msprintf(gettext("%s: Wrong number of output argument(s): %d or %d expected.\n"),"modbus_writeBit", 1, 2));
    end

    if rhs <> 3 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"),"modbus_writeBit", 3));
    end

    if size(value, "*") <> 1 then
        error(msprintf(_("%s:  Wrong value for input argument #%d: a scalar expected.\n"), "modbus_writeBit"), 3);
    end

    [retVal, ierr] = modbus_writeBits(modbusContext, address, value);
    varargout(2) = ierr;
    if ierr <> 0 then
        varargout(1) = [];
    else
        varargout(1) = retVal;
    end
endfunction
