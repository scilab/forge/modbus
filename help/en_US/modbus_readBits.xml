<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="modbus_readBits">
  <refnamediv>
    <refname>modbus_readBits</refname>

    <refpurpose>read many bits</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>
res = modbus_readBits(mbc, addr, nb)
[res, ierr] = modbus_readBits(mbc, addr, nb)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>mbc</term>
        <listitem>
          <para>The Modbus connection created with modbus_newTcp or modbus_newRtu.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>addr</term>
        <listitem>
          <para>The address on device where bits should be read from.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>nb</term>
        <listitem>
          <para>The number of bits that should be read.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>res</term>
        <listitem>
          <para>A double matrix (size 1 x nb) with coils values. An empty matrix in case of error when called with two output arguments.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ierr</term>
        <listitem>
          <para>Erreur number.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function reads the status of the <literal>nb</literal> bits (coils) to
    the address <literal>addr</literal> of the remote device through <literal>mbc</literal> connection
    descriptor.</para>

    <para>The function uses the Modbus function code 0x01 (read coil status).</para>

    <para>When called with one output argument, a Scilab error will occur when the reading fails. When called with two output arguments, no Scilab error will occur but <literal>ierr</literal> will contain the internal modbus error number.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[
    mbc = modbus_newTcp("127.0.0.1", 502);
    modbus_connect(mbc);
    res = modbus_readBits(mbc, 0, 8);
    modbus_close(mbc);
    modbus_free(mbc);
    ]]></programlisting>
  </refsection>
  <refsection role="see also">
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="modbus_newTcp">modbus_newTcp</link>
      </member>
      <member>
        <link linkend="modbus_connect">modbus_connect</link>
      </member>
      <member>
        <link linkend="modbus_writeBits">modbus_writeBits</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
