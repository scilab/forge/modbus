
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//

demopath = get_absolute_file_path("modbus.dem.gateway.sce");
subdemolist = ["modbus"             ,"modbus.dem.sce"];
subdemolist(:,2) = demopath + subdemolist(:,2);
clear demopath;
